import json, time
from pprint import pprint
from tqdm import tqdm
import tableprint
import chalk
import click
from click import echo
import win32serviceutil

#                   0       1           2           3           4           5           6          7
STATUS_FROM_CODE = ['', 'Stopped', 'Starting', 'Stopping', 'Running', 'Continuing', 'Pausing', 'Paused']
STATUS_COLOR =     ['', 'red',     'blue',     'blue',     'green',   'blue',       'blue',    'grey']

with open('config.json', 'r') as f:
    config = json.load(f)

ALL_SERVICES, PROFILES = config['allServices'], config['profiles']
TABLE_WIDTH = len(max(ALL_SERVICES, key=len))

@click.group()
def cli():
    pass

@cli.command()
def status():
    table_data = []
    current_profile = 'custom'
    running_services = set()
    with click.progressbar(ALL_SERVICES) as services:
        for service in services:
            status_code = win32serviceutil.QueryServiceStatus(service)[1]
            status = chalk.Chalk(STATUS_COLOR[status_code])(STATUS_FROM_CODE[status_code])
            table_data.append([service.ljust(TABLE_WIDTH), status.ljust(TABLE_WIDTH)])
            if status_code == 4:
                running_services.add(service)
            time.sleep(0.05)
    for (profile_name, profile_data) in PROFILES.items():
        if set(profile_data['services']) == running_services:
            current_profile = profile_name
    echo(chalk.cyan('\n Current Profile: ' + current_profile, bold=True))
    tableprint.table(table_data, headers=['Service', 'Status'], width=TABLE_WIDTH, style='round')

@cli.command()
@click.argument('service')
def start(service):
    try:
        print('Starting ' + service + '...')
        win32serviceutil.StartService(service)
        echo('Started ' + service + '.')
    except Exception as e:
        if e.args[0] in (1056, 1060):
            echo(e.args[2])
        else:
            echo(e.args)
            raise e

@cli.command()
@click.argument('service')
def stop(service):
    try:
        print('Stopping ' + service + '... ', end='')
        win32serviceutil.StopService(service)
        echo('Done')
    except Exception as e:
        if e.args[0] in (1062, 1060):
            echo(e.args[2])
        else:
            echo(e.args)
            raise e

@cli.command()
@click.argument('profile')
def switch(profile):
    echo('Switching to ' + profile)
    if profile.lower() not in PROFILES:
        echo('Profile does not exist.')
        return
    for service in ALL_SERVICES:
        try:
            status_code = win32serviceutil.QueryServiceStatus(service)[1]
            if service not in PROFILES[profile]['services']:
                win32serviceutil.StopService(service)
                echo('Stopped '+service)
            else:
                win32serviceutil.StartService(service)
                echo('Started '+service)
        except Exception as e:
            if e.args[0] in (1056, 1062, 1060):
                pass
            elif e.args[0] == 5:
                echo(chalk.red('Access is denied. Run as admin.'))
                return
            else:
                echo(e.args)
                raise e
    echo(chalk.green('Done.'))


if __name__ == '__main__':
    cli()
